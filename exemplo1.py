'''
Created on 13 de jan de 2018

@author: Ricardo Santos
'''

import numpy as np
import random
import time
import cProfile

def somaVetores1(vetor1, vetor2):
    # Inicializa o vetor resultado
    resultado = np.zeros(len(vetor1))
    
    # ENQUANTO i for menor que o numero de posicoes
    for i in range(len(vetor1)):
        # Atribui a posicao i da variavel resultado a soma do i-esimo elemento de cada vetor
        resultado[i] = vetor1[i] + vetor2[i]
    # FIM ENQUANTO
    
    # Retorna a variavel com as somas
    return resultado

def inicializaVetorAleatorio(posicoes):
    # Inicializa o vetor com valores zero
    resultado = np.zeros(posicoes)
    
    # ENQUANTO i for menor que o numero de posicoes
    for i in range(posicoes):
        resultado[i] = random.random()
    # FIM ENQUANTO
    
    # Retorna a variavel inicializada
    return resultado

def principal():
    # Pede para o usuario a quantidade de posicoes no vetor
    numeroDePosicoes = 100000 # input("Quantidade de posicoes no vetor:\n")
    
    # Inicializa o vetor 1 com a quantidade de posicoes escolhida
    vetor1 = inicializaVetorAleatorio(numeroDePosicoes)
    
    # Inicializa o vetor 2 com a quantidade de posicoes escolhida
    vetor2 = inicializaVetorAleatorio(numeroDePosicoes)
    
    # Inicializa o vetor 3 com a quantidade de posicoes escolhida
    vetor3 = somaVetores1(vetor1, vetor2)
    
    # Mostra para o usuario o resultado da soma
    # print vetor3
    
if __name__  == '__main__':
    cProfile.run('principal()')